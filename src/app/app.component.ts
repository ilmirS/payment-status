import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MONTHS } from './models/const';
import { Payment } from './models/payment';
import { PaymentService } from './services/payment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  months = MONTHS;
  paymentsForm: FormGroup;
  paymentForm: FormGroup;
  payments: Payment[];
  total = 0;

  constructor(private fb: FormBuilder, private paymentService: PaymentService) {
  }

  ngOnInit() {
    this.paymentService.getAllPayment().subscribe(data => {
      this.payments = data;
      this.paymentsForm = this.fb.group({
        payments: this.fb.array(this.payments.map(value => {
          return {
            name: value.name,
            price: value.price,
            months: this.fb.array(value.months)
          };
        }))
      });
      this.updateTotal();
    });
    this.paymentForm = this.fb.group({
      paymentName: ['', Validators.required],
      paymentPrice: ['', [Validators.required, Validators.min(0)]]
    });
  }

  addPayment() {
    (this.paymentsForm.controls.payments as FormArray).push(new FormControl({
      name: this.paymentForm.controls.paymentName.value,
      price: this.paymentForm.controls.paymentPrice.value,
      months: this.fb.array(this.months.map(() => false))
    }));
    this.paymentForm.reset();
  }

  removePayment(index) {
    (this.paymentsForm.controls.payments as FormArray).removeAt(index);
    this.updateTotal();
  }

  updateTotal() {
    const year = new Date().getFullYear();
    this.total = this.paymentsForm.controls.payments.value.reduce((total, payment) => {
      payment.months.value.forEach((value, index) => {
        if (value) {
          total += payment.price * (32 - new Date(year, index, 32).getDate());
        }
      });
      return total;
    }, 0);
  }
}
