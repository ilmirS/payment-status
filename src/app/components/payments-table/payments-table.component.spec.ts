import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsTableComponent } from './payments-table.component';
import { FormArray, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { MONTHS } from '../../models/const';

describe('PaymentsTableComponent', () => {
  let component: PaymentsTableComponent;
  let fixture: ComponentFixture<PaymentsTableComponent>;
  let spy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgZorroAntdModule, FormsModule, ReactiveFormsModule],
      declarations: [PaymentsTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsTableComponent);
    component = fixture.componentInstance;
    component.months = MONTHS;
    component.paymentsForm = new FormGroup({
      payments: new FormArray([
        new FormControl({
          name: 'TV',
          price: 10,
          months: new FormArray(component.months.map(() => new FormControl([false])))
        })
      ])
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit removePayment', () => {
    spy = spyOn(component.removePayment, 'emit');
    component.clickRemovePayment(1);
    expect(spy.calls.any()).toBeTruthy();
  });

  it('should emit removePayment', () => {
    spy = spyOn(component.updateTotal, 'emit');
    component.clickUpdateTotal();
    expect(spy.calls.any()).toBeTruthy();
  });
});
