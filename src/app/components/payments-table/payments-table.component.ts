import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-payments-table',
  templateUrl: './payments-table.component.html',
  styleUrls: ['./payments-table.component.scss']
})
export class PaymentsTableComponent {

  @Input() paymentsForm: FormGroup;
  @Input() months;
  @Output() removePayment = new EventEmitter<number>();
  @Output() updateTotal = new EventEmitter();

  constructor() {
  }

  clickRemovePayment(index) {
    this.removePayment.emit(index);
  }

  clickUpdateTotal() {
    this.updateTotal.emit();
  }
}
