import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.scss']
})
export class AddPaymentComponent {

  @Input() paymentForm: FormGroup;
  @Output() addPayment = new EventEmitter();

  constructor() {
  }

  clickAddPayment() {
    this.addPayment.emit();
  }

}
