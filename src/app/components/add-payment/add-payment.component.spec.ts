import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaymentComponent } from './add-payment.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('AddPaymentComponent', () => {
  let component: AddPaymentComponent;
  let fixture: ComponentFixture<AddPaymentComponent>;
  let submitEl: DebugElement;
  let spy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgZorroAntdModule, FormsModule, ReactiveFormsModule],
      declarations: [AddPaymentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaymentComponent);
    component = fixture.componentInstance;
    component.paymentForm = new FormGroup({
        paymentName: new FormControl('', Validators.required),
        paymentPrice: new FormControl('', Validators.required)
      }
    );
    submitEl = fixture.debugElement.query(By.css('button'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('the button should be disabled if the form is not valid', () => {
    expect(submitEl.nativeElement.disabled).toBeTruthy();
  });

  it('should emit addPayment', () => {
    spy = spyOn(component.addPayment, 'emit');
    component.clickAddPayment();
    expect(spy.calls.any()).toBeTruthy();
  });
});
