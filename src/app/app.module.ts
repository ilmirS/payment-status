import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, ru_RU } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import ru from '@angular/common/locales/ru';
import { AddPaymentComponent } from './components/add-payment/add-payment.component';
import { PaymentsTableComponent } from './components/payments-table/payments-table.component';
import { PaymentService } from './services/payment.service';

registerLocaleData(ru);

@NgModule({
  declarations: [
    AppComponent,
    AddPaymentComponent,
    PaymentsTableComponent
  ],
  imports: [
    BrowserModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: ru_RU },
    PaymentService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
