import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AddPaymentComponent } from './components/add-payment/add-payment.component';
import { PaymentsTableComponent } from './components/payments-table/payments-table.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { Payment } from './models/payment';
import { HttpClientModule } from '@angular/common/http';
import { PaymentService } from './services/payment.service';
import { MONTHS } from './models/const';

describe('AppComponent', () => {

  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let paymentService: PaymentService;
  let spy: jasmine.Spy;
  let mockPayments: Payment[];
  let paymentName;
  let paymentPrice;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgZorroAntdModule, FormsModule, ReactiveFormsModule, HttpClientModule],
      declarations: [AppComponent, AddPaymentComponent, PaymentsTableComponent],
      providers: [PaymentService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    app.months = MONTHS;
    paymentService = fixture.debugElement.injector.get(PaymentService);
    mockPayments = [
      new Payment('Интернет', 60, [false, false, false, false, false, false, false, false, false, false, false, false]),
      new Payment('Домашний телефон', 50, [false, true, false, false, false, false, false, false, false, false, false, false]),
    ];
    spy = spyOn(paymentService, 'getAllPayment').and.returnValue(of(mockPayments));
    fixture.detectChanges();
    paymentName = app.paymentForm.controls['paymentName'];
    paymentPrice = app.paymentForm.controls['paymentPrice'];
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should call paymentService', () => {
    app.ngOnInit();
    expect(spy.calls.any()).toBeTruthy();
  });

  it('should set payments', () => {
    expect(app.payments).toEqual(mockPayments);
  });

  it('is paymentForm valid', () => {
    paymentName.setValue('IPTV');
    paymentPrice.setValue(100);
    expect(app.paymentForm.valid).toBeTruthy();
  });

  it('is paymentForm invalid when Name empty', () => {
    paymentName.setValue('');
    paymentPrice.setValue(100);
    expect(app.paymentForm.valid).toBeFalsy();
    expect(app.paymentForm.controls['paymentName'].valid).toBeFalsy();
  });

  it('is paymentForm invalid when Price empty', () => {
    paymentName.setValue('IPTV');
    paymentPrice.setValue('');
    expect(app.paymentForm.valid).toBeFalsy();
    expect(app.paymentForm.controls['paymentPrice'].valid).toBeFalsy();
  });

  it('is paymentForm invalid when Price less 0', () => {
    paymentName.setValue('IPTV');
    paymentPrice.setValue(-100);
    expect(app.paymentForm.valid).toBeFalsy();
    expect(app.paymentForm.controls['paymentPrice'].valid).toBeFalsy();
  });

  it('should update Total', () => {
    spy = spyOn(app, 'updateTotal').and.returnValue(0);
    app.updateTotal();
    expect(spy.calls.any()).toBeTruthy();
  });

  it('should add Payment', () => {
    paymentName.setValue('IPTV');
    paymentPrice.setValue(100);
    spy = spyOn(app, 'addPayment');
    app.addPayment();
    expect(spy.calls.any()).toBeTruthy();
  });

  it('should remove Payment', () => {
    spy = spyOn(app, 'removePayment');
    app.removePayment(0);
    expect(spy.calls.any()).toBeTruthy();
  });

});
