export class Payment {
  constructor(
    public name: string,
    public price: number,
    public months: boolean[],
  ) {
  }
}
