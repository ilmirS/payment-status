import { TestBed } from '@angular/core/testing';

import { PaymentService } from './payment.service';
import { Payment } from '../models/payment';

describe('PaymentService', () => {
  let service: PaymentService;
  const mockPayments: Payment[] = [new Payment('Интернет', 60,
    [false, false, true, false, false, false, false, false, false, false, false, false])];

  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => {
    service = TestBed.get(PaymentService);
    service.data = mockPayments;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return getAllPayment', () => {
    service.getAllPayment().subscribe((data) => {
      expect(data).toBe(mockPayments);
    });
  });

});
