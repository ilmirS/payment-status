import { Injectable } from '@angular/core';
import { Payment } from '../models/payment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  public data: Payment[] = [
    new Payment('Интернет', 60, [false, false, true, false, false, false, false, false, false, false, false, false]),
    new Payment('Домашний телефон', 50, [false, true, false, true, false, false, false, false, false, false, false, false]),
    new Payment('Мобильный телефон', 30, [false, false, false, false, true, false, false, false, false, false, false, false]),
    new Payment('IPTV', 20, [false, false, false, false, true, false, true, false, false, false, false, false]),
    new Payment('Подписка на музыку', 15, [false, false, false, false, false, false, false, false, false, false, false, false]),
    new Payment('Подписка на фильмы', 15, [false, false, false, false, true, false, false, false, false, false, false, false]),
    new Payment('Абонимент на фитнес', 100, [false, false, false, false, false, false, false, false, false, false, false, false]),
  ];

  constructor() {
  }

  getAllPayment(): Observable<Array<Payment>> {
    return of(this.data);
  }
}
